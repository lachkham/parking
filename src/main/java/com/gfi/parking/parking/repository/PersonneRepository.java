package com.gfi.parking.parking.repository;

import com.gfi.parking.parking.entities.Personne;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;
@Repository
public interface PersonneRepository extends JpaRepository<Personne,String> {
    Personne findByUsernameAndPassword(String username,String password);
    Personne findByUsername(String username);
    Optional<Personne> findByCin(String CIN);
    boolean existsByUsername(String username);

    List<Personne> findAllBySuperior(String superior);

}
