package com.gfi.parking.parking.repository;

import com.gfi.parking.parking.entities.TypeParking;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TypeParkingRepository extends JpaRepository<TypeParking,Long> {
}
