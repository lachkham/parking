package com.gfi.parking.parking.repository;


import com.gfi.parking.parking.entities.Reservation;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ReservationRepository extends JpaRepository<Reservation,Long> {


    @Query("SELECT count(r) FROM Reservation r WHERE (r.idPark=?1 and ((r.entre<?2 and ?2<r.sortie) or (r.entre<?3 and ?3<r.sortie ) or (?2<r.entre and r.sortie<?3)))")
    Long countParking(Long parking,Long entre, Long sortie);

    @Query("SELECT count(r) FROM Reservation r WHERE ((r.entre<?1 and ?1<r.sortie) or (r.entre<?2 and ?2<r.sortie ) or (?1<r.entre and r.sortie<?2))")
    Long countParkings(Long entre, Long sortie);

    @Query("SELECT count(DISTINCT r.idPark) FROM Reservation r WHERE ((r.entre<?1 and ?1<r.sortie) or (r.entre<?2 and ?2<r.sortie ) or (?1<r.entre and r.sortie<?2)) and " +
                                                                        "r.idPark in (SELECT p.idParking from Parking p where p.owner=?3)")
    Long countParkinActifs(Long entre, Long sortie,String user);


    @Query("select r from Reservation r where r.idPark in (SELECT p.idParking from Parking p where p.owner=?1) ORDER BY r.idReservation DESC ")
    List<Reservation> findAllByParkingOwner(String User);


}


