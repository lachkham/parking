package com.gfi.parking.parking.repository;

import com.gfi.parking.parking.entities.Parking;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface    ParkingRepository extends JpaRepository<Parking,Long> {
     List<Parking> findAllByValidTrue();
     List<Parking> findAllByValidTrueAndOwner(String owner);
     List<Parking> findAllByValidFalseAndOwner(String owner);
     Long          countByValidFalseAndOwner(String owner);
     Long          countByValidTrueAndOwner(String owner);

     Parking findByLongitudeBetweenAndLatitudeBetween(double LongInf, double LongSup,double LattInf, double LattSup);

     @Query("SELECT sum (p.nbPlace) FROM Parking p where p.valid=true AND p.owner=?1")
     Long TotalPlace(String owner);


}
