package com.gfi.parking.parking.security;

import com.gfi.parking.parking.entities.Personne;
import com.gfi.parking.parking.repository.PersonneRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
public class UserSecurityService implements UserDetailsService {
    private static final Logger LOG = LoggerFactory.getLogger(UserSecurityService.class);

    @Autowired
    private PersonneRepository userRepository;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {

        Personne user = userRepository.findByUsername(username);

        if(user == null){
            LOG.warn("Username" + username + " not found");
            throw new UsernameNotFoundException("Username " + username + " not found!");
        }
        System.out.println(user);
        return user;
    }


}
