package com.gfi.parking.parking.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.session.web.http.HeaderHttpSessionStrategy;
import org.springframework.session.web.http.HttpSessionStrategy;


@Configuration
@EnableWebSecurity

public class SecurityConfig extends WebSecurityConfigurerAdapter{


    @Autowired
    private UserSecurityService userSecurityService;

    private BCryptPasswordEncoder passwordEncoder(){
        return SecurityUtility.passwordEncoder();
    }


    @Override
    public void configure(WebSecurity web) throws Exception {

    web.ignoring()
            .antMatchers("/v2/api-docs")//
            .antMatchers("/swagger-resources/**")//
            .antMatchers("/swagger-ui.html")//
            .antMatchers("/configuration/**")//
            .antMatchers("/webjars/**")//

            .antMatchers("/#/**")
            .antMatchers("/")
            .antMatchers("/#/home")
            .antMatchers("/#/login/**")
            .antMatchers("/#/admin/**")
            .antMatchers("/styles**")
            .antMatchers("/runtime**")
            .antMatchers("/polyfills**")
            .antMatchers("/favicon.ico**")
            .antMatchers("/main**")
            .antMatchers("/assets/**")

    ;

        web.ignoring().antMatchers("/files/**");
    }
    private static final String[] PUBLIC_Get_MATCHERS = {
            "/v1/adresses/**",
            "/v1/parking/Get/**",
            "/v1/Type/**",
            "/v1/reservation/Get/**",




    };
    private static final String[] PUBLIC_Post_MATCHERS = {
           "/v1/adresses/**",
            "/v1/parking/**",
            "/v1/reservation/**",
            "/v1/personnes/"

    };
    @Override
    protected void configure(HttpSecurity http) throws Exception{

        http
                .csrf().disable().cors()
                .disable()

                .httpBasic().and()
                .authorizeRequests()
                .antMatchers(HttpMethod.GET,PUBLIC_Get_MATCHERS).permitAll()

                .antMatchers(HttpMethod.POST,PUBLIC_Post_MATCHERS).permitAll()
                .anyRequest().authenticated()
        ;



      /*  http
                .httpBasic()
                .and()
                .authorizeRequests()
                .antMatchers(HttpMethod.GET, "/v1/reservation/**").permitAll()
                .antMatchers(HttpMethod.POST, "/books").hasRole("ADMIN")
                .antMatchers(HttpMethod.PUT, "/books/**").hasRole("ADMIN")
                .antMatchers(HttpMethod.PATCH, "/books/**").hasRole("ADMIN")
                .antMatchers(HttpMethod.DELETE, "/books/**").hasRole("ADMIN")
                .and()
                .csrf().disable()
                .formLogin().disable();*/
    }

    @Autowired
    public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(userSecurityService).passwordEncoder(new BCryptPasswordEncoder());
    }

    @Bean
    public HttpSessionStrategy httpSessionStrategy() {

        return new HeaderHttpSessionStrategy();
    }
}
