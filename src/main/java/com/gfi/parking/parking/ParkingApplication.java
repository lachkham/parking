package com.gfi.parking.parking;

import com.gfi.parking.parking.entities.Personne;
import com.gfi.parking.parking.security.SecurityUtility;
import com.gfi.parking.parking.security.UserSecurityService;
import com.gfi.parking.parking.service.PersonneService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.FilterType;
import org.springframework.security.core.userdetails.User;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@SpringBootApplication
@ComponentScan(excludeFilters = @ComponentScan.Filter(type = FilterType.ASSIGNABLE_TYPE))
@EnableSwagger2
public class ParkingApplication  {


	@Autowired
	private PersonneService userService;

	@Autowired
	private UserSecurityService userSecurityService;



	public static void main(String[] args) {
		SpringApplication.run(ParkingApplication.class, args);
	}
	@Bean
	public Docket productApi() {
		return new Docket(DocumentationType.SWAGGER_2).select().apis(RequestHandlerSelectors.basePackage("com.gfi.parking.parking"))
				.build();
	}




}
