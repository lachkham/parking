package com.gfi.parking.parking.DTO;

import lombok.Data;

@Data
public class TypeParkingDTO {
    private long typeId;
    private String type;
}
