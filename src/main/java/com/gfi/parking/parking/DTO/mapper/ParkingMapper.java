package com.gfi.parking.parking.DTO.mapper;

import com.gfi.parking.parking.DTO.ParkingDTO;
import com.gfi.parking.parking.entities.Adresse;
import com.gfi.parking.parking.entities.Parking;
import com.gfi.parking.parking.entities.TypeParking;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;

@Mapper(componentModel = "spring", uses = {Adresse.class, TypeParking.class})
public interface ParkingMapper extends EntityMapper<ParkingDTO, Parking> {

    @Mappings({
            @Mapping(source =  "adresse.idAdr",         target = "idAdr"),
            @Mapping(source =  "type.typeId",           target = "typeId"),
            @Mapping(source =  "owner",           target = "owner")

    })
    ParkingDTO toDto(Parking entityList);

    @Mappings({
            @Mapping(source =  "idAdr", target = "adresse.idAdr"),
            @Mapping(source =  "typeId", target = "type.typeId"),
            @Mapping(source =  "owner",           target = "owner")

    })

    Parking toEntity(ParkingDTO dto);


    default Parking fromId(Long id) {
        if (id == null) {
            return null;
        }
        Parking generated = new Parking();
        generated.setIdParking(id);
        return generated;
    }

}
