package com.gfi.parking.parking.DTO;
import lombok.Data;
@Data
public class ReservationDTO {
    private Long idReservation;
    private Long entre;
    private Long sortie;
    private Long idParking   ;
    private String email;

}