package com.gfi.parking.parking.DTO.mapper;

import com.gfi.parking.parking.DTO.ReservationDTO;
import com.gfi.parking.parking.entities.Parking;
import com.gfi.parking.parking.entities.Reservation;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;


@Mapper(componentModel = "spring", uses = {Parking.class})
public interface ReservationMapper extends EntityMapper<ReservationDTO,Reservation>{

    @Mappings({
            @Mapping(source ="idPark", target = "idParking"),
            @Mapping(source ="email", target = "email"),


    })
    ReservationDTO toDto(Reservation entity);


    @Mappings({
            @Mapping(source ="idParking",target = "idPark"),
            @Mapping(source ="email", target = "email"),

    })
    Reservation toEntity(ReservationDTO dto);



    default Reservation fromId(Long id) {
        if (id == null) {
            return null;
        }
        Reservation generated = new Reservation();
        generated.setIdReservation(id);
        return generated;
    }
}
