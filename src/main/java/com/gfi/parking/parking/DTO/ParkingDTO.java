package com.gfi.parking.parking.DTO;

import lombok.Data;

@Data
public class ParkingDTO {

    private     Long        idParking ;
    private     String      libelle;
    private     double      longitude;
    private     double      latitude;
    private     int         nbPlace;
    //private     boolean     valid;
    private     String         owner;
    //Adresse
    private     Long        idAdr;
    //Type Parking
    private     long        typeId;
}
