package com.gfi.parking.parking.DTO.mapper;


import com.gfi.parking.parking.DTO.TypeParkingDTO;
import com.gfi.parking.parking.entities.TypeParking;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface TypeParkingMapper extends EntityMapper<TypeParkingDTO, TypeParking>{

    TypeParking toEntity(TypeParkingDTO typeParkingDTO);

    TypeParkingDTO toDto (TypeParking typeParking);

    default TypeParking fromId(Long id) {
        if (id == null) {
            return null;
        }
        TypeParking generated = new TypeParking();
        generated.setTypeId(id);
        return generated;
    }


}
