package com.gfi.parking.parking.DTO;

import lombok.Data;

import java.util.Date;

@Data
public class PersonneDTO {

    private String cin ;
    /*@Lob
    @Nullable
    private byte[] photo;*/
    private String prenom;
    private String nom;
    private Date DateNaiss;

    private String username;
    private String password;
    private String Email;
    private String superior;

    //Adresse
    private Long   idAdr;
    private boolean status;



}
