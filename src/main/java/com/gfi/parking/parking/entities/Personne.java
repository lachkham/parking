package com.gfi.parking.parking.entities;



import com.gfi.parking.parking.security.Authority;
import lombok.Data;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import javax.validation.constraints.Email;

import javax.persistence.*;
import java.io.Serializable;
import java.util.*;

@Entity
@Data
public class Personne implements UserDetails, Serializable {

    @Id
    private String cin ;
    /*@Lob
    @Nullable
    private byte[] photo;*/
    private String prenom;
    private String nom;
    private Date DateNaiss;


    @ManyToOne
    private Adresse adresse;

    private String username;
    private String password;

    private String superior;

    @Email(message = "Email should be valid")
    private String Email;


    @Column(columnDefinition = "boolean default true")
    private boolean status;



    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        Set<GrantedAuthority> authorities = new HashSet<>();
        authorities.add(new Authority("ADMIN"));
        return authorities;
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return this.status;
    }
}
