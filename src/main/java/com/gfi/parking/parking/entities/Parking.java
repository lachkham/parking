package com.gfi.parking.parking.entities;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;


@Entity
@Data
public class Parking implements Serializable {
    @Id
    @GeneratedValue
    private     Long        idParking ;
    private     String      libelle;
    private     double      longitude;
    private     double      latitude;
    private     int         nbPlace;


    private     String      owner;

    @Column(columnDefinition = "boolean default false")
    private     boolean     valid;

    @ManyToOne
    private     Adresse     adresse;
    @ManyToOne
    private     TypeParking type;
}
