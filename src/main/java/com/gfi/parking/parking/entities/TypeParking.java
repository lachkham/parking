package com.gfi.parking.parking.entities;

import lombok.Data;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.io.Serializable;

@Entity
@Data
public class TypeParking implements Serializable {
    @Id
    @GeneratedValue
    private long typeId;
    private String type;
}
