package com.gfi.parking.parking.entities;

import lombok.Data;

import javax.persistence.*;
import javax.validation.constraints.Email;
import java.io.Serializable;

@Entity
@Data
public class Reservation implements Serializable {

    @Id
    @GeneratedValue
    private Long idReservation;
    private Long entre;
    private Long sortie;
    private Long idPark;
    @Email(message = "Email should be valid")
    @Column(columnDefinition = "VARCHAR(40) default 'lachkam55@gmail.com'")
    private String email;
   /* @ManyToOne
    private  Personne personne;*/


}
