package com.gfi.parking.parking.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

@Entity
@Data

public class Adresse  implements Serializable {
    @Id
    @GeneratedValue
    private Long   idAdr;
    private String Adresse;
    private String codePostal;
    private String Ville;
    private String Gouvernorat;
    private String pays;





}
