package com.gfi.parking.parking.service;

import com.gfi.parking.parking.DTO.TypeParkingDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Optional;

public interface TypeParkingService {
    TypeParkingDTO save(TypeParkingDTO typeParkingDTO);
    List<TypeParkingDTO> findAll();
    Optional<TypeParkingDTO> findOne(Long id);
    void delete(Long id);
}
