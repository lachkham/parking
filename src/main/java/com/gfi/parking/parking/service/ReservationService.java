package com.gfi.parking.parking.service;


import com.gfi.parking.parking.DTO.ReservationDTO;

import java.util.List;
import java.util.Optional;

public interface ReservationService {


    ReservationDTO save(ReservationDTO reservationDTO);
    List<ReservationDTO> findAll(String user);
    Optional<ReservationDTO> findOne(Long id);
    Optional<Long> nbReservation(Long idPark,Long entre,Long sortie);
    void delete(Long id);
    Optional<Long>countParkinActifs(Long entre,Long sortie,String user);


}
