package com.gfi.parking.parking.service.impl;

import com.gfi.parking.parking.DTO.PersonneDTO;
import com.gfi.parking.parking.DTO.mapper.PersonneMapper;
import com.gfi.parking.parking.entities.Personne;
import com.gfi.parking.parking.repository.PersonneRepository;
import com.gfi.parking.parking.service.PersonneService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;


@Service
@Transactional
public class PersonneServiceImpl implements PersonneService {

    private static final Logger LOG = LoggerFactory.getLogger(PersonneService.class);
    @Autowired
    private PasswordEncoder passwordEncoder;


    private final PersonneMapper personneMapper;
    private final PersonneRepository personneRepository;

    public PersonneServiceImpl(PersonneMapper personneMapper, PersonneRepository personneRepository) {
        this.personneMapper = personneMapper;
        this.personneRepository = personneRepository;
    }


    @Override
    public PersonneDTO save(PersonneDTO personneDTO) {
        Personne localUser = personneRepository.findByUsername(personneDTO.getUsername());
        personneDTO.setPassword(passwordEncoder.encode(personneDTO.getPassword()));
        if(localUser != null) {
            if(localUser.getCin().equals(personneDTO.getCin())){
                localUser = personneRepository.save(personneMapper.toEntity(personneDTO));
            }else
                return null;
        }else {
            localUser = personneRepository.save(personneMapper.toEntity(personneDTO));

        }

        return personneMapper.toDto(localUser);


    }

    @Override

    public List<PersonneDTO> findAll(String sup) {
        return personneMapper.toDto(personneRepository.findAllBySuperior(sup));

    }

    @Override
    public Optional<PersonneDTO> findOne(String id) {
        return Optional.of(personneMapper.toDto(personneRepository.findById(id).get()));
    }

    @Override
    public void delete(String id) {
        personneRepository.deleteById(id);
    }

    @Override
    public Optional<PersonneDTO> findByUsername(String username) {

        return Optional.of(personneMapper.toDto(personneRepository.findByUsername(username)));
    }




}