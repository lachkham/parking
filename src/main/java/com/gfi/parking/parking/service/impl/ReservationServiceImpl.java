package com.gfi.parking.parking.service.impl;

import com.gfi.parking.parking.DTO.ReservationDTO;
import com.gfi.parking.parking.DTO.mapper.ReservationMapper;
import com.gfi.parking.parking.entities.Parking;
import com.gfi.parking.parking.repository.ParkingRepository;
import com.gfi.parking.parking.repository.ReservationRepository;
import com.gfi.parking.parking.service.ReservationService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class ReservationServiceImpl implements ReservationService {
    private final ReservationMapper reservationMapper;
    private final ReservationRepository reservationRepository;

    public ReservationServiceImpl(ReservationMapper reservationMapper, ReservationRepository reservationRepository) {
        this.reservationMapper = reservationMapper;
        this.reservationRepository = reservationRepository;
    }



    @Override
    public ReservationDTO save(ReservationDTO reservationDTO) {
        return reservationMapper.toDto(reservationRepository.save(reservationMapper.toEntity(reservationDTO)));
    }



    @Override
    public List<ReservationDTO> findAll(String user) {
        return reservationMapper.toDto(reservationRepository.findAllByParkingOwner(user));
    }

    @Override
    public Optional<ReservationDTO> findOne(Long id) {
        return Optional.of(reservationMapper.toDto(reservationRepository.findById(id).get()));
    }

    @Override
    public Optional<Long> nbReservation(Long idPark,Long entre, Long sortie) {
        if (idPark==-1){
            if (entre==-1 || sortie == -1){
                return Optional.of(reservationRepository.count());
            }else{
                return Optional.of(reservationRepository.countParkings(entre,sortie));
            }
        }
        return Optional.of(reservationRepository.countParking(idPark,entre,sortie));
    }

    @Override
    public void delete(Long id) {
        reservationRepository.deleteById(id);

    }

    @Override
    public Optional<Long> countParkinActifs(Long entre, Long sortie,String user) {
        return Optional.ofNullable(reservationRepository.countParkinActifs(entre,sortie,user));
    }
}
