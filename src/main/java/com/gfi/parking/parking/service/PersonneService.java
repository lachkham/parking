package com.gfi.parking.parking.service;

import com.gfi.parking.parking.DTO.PersonneDTO;
import com.gfi.parking.parking.entities.Personne;

import java.util.List;
import java.util.Optional;

public interface PersonneService {
    PersonneDTO save(PersonneDTO personneDTO);
    List<PersonneDTO> findAll(String Superior);
    Optional<PersonneDTO> findOne(String id);
    void delete(String id);
    Optional<PersonneDTO>  findByUsername (String username);
}
