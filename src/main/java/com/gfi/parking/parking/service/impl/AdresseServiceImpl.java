package com.gfi.parking.parking.service.impl;

import com.gfi.parking.parking.repository.AdresseRepository;
import com.gfi.parking.parking.service.AdresseService;
import com.gfi.parking.parking.DTO.AdresseDTO;
import com.gfi.parking.parking.DTO.mapper.AdresseMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;


@Service
@Transactional
public class AdresseServiceImpl implements AdresseService {


    private final AdresseRepository adresseRepository;

    private  final AdresseMapper adresseMapper;

    private final Logger log =LoggerFactory.getLogger(AdresseServiceImpl.class);


    public AdresseServiceImpl(AdresseRepository adresseRepository, AdresseMapper adresseMapper) {
        this.adresseRepository = adresseRepository;
        this.adresseMapper = adresseMapper;
    }

    /**
     * Save an Address.
     *
     * @param adresseDTO the entity to save
     * @return the persisted entity
     */
    @Override
    public AdresseDTO save(AdresseDTO adresseDTO) {
        log.debug("Enregistrer AdresseDTO : {}",adresseDTO);
        return adresseMapper.toDto(adresseRepository.save(adresseMapper.toEntity(adresseDTO)));
    }
    /**
     * Get all the Addresses.
     *
     * @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public List<AdresseDTO> findAll() {
        log.debug("find All : AdresseDTO");
        return adresseMapper.toDto(adresseRepository.findAll());


    }

    /**
     * Get one Address by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<AdresseDTO> findOne(Long id) {
        log.debug("find one : AdresseDTO");
        return Optional.of(adresseMapper.toDto(adresseRepository.findById(id).get()));
    }
    /**
     * Delete the Address by id.
     *
     * @param id the id of the entity
     */

    @Override
    public void delete(Long id) {
        log.debug("Delete {} AdresseDTO",id);
        adresseRepository.deleteById(id);
    }
}
