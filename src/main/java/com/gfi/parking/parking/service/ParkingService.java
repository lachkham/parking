package com.gfi.parking.parking.service;

import com.gfi.parking.parking.DTO.ParkingDTO;

import java.util.List;
import java.util.Optional;

public interface ParkingService {
    ParkingDTO save(ParkingDTO parkingDTO);
    List<ParkingDTO> findAll();
    List<ParkingDTO> findAllOwner(String owner);
    List<ParkingDTO> findAllinvalid(String owner);
    Optional<ParkingDTO> findOne(Long id);
    void delete(Long id);
    Optional<ParkingDTO> findByLongitudeAndLatitude(double longitude, double latitude);
    Optional<Long> countByInvalidParkings(String owner);
    Optional<ParkingDTO>  valider(Long id);
    Optional<Long> totalPlace(String owner);
    Optional<Long> countValidParking(String owner);



}
