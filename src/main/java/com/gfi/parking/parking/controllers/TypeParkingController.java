package com.gfi.parking.parking.controllers;
import com.gfi.parking.parking.DTO.TypeParkingDTO;
import com.gfi.parking.parking.service.TypeParkingService;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;


@RestController
@RequestMapping("/v1/Type")
@CrossOrigin(origins = "*")
public class TypeParkingController {

private final TypeParkingService typeParkingService;

    public TypeParkingController(TypeParkingService typeParkingService) {
        this.typeParkingService = typeParkingService;
    }


    @GetMapping("/")
    public ResponseEntity findAll(){
        return ResponseEntity.ok(typeParkingService.findAll());

    }
    @GetMapping("/{idType}")
    public ResponseEntity findTypeByID(@PathVariable Long idType){
        if (idType== null){
            return ResponseEntity.badRequest().body("Null Identifier");
        }
        Optional<TypeParkingDTO> typeParking = typeParkingService.findOne(idType);
        if (!typeParking.isPresent())
            return ResponseEntity.notFound().build();

        return ResponseEntity.ok(typeParking);
    }

    @PostMapping("/")
    public ResponseEntity createTypeParking(@RequestBody TypeParkingDTO type){
        if(type==null){
            ResponseEntity.badRequest().body("Empty Request Body");
        }

        TypeParkingDTO typeParking = typeParkingService.save(type);

        return ResponseEntity.ok(typeParking);
    }

    @DeleteMapping("/{idType}")
    public ResponseEntity deleteTypeParking(@PathVariable Long idType){
        if (idType==null){
            return ResponseEntity.badRequest().body("Null id Type");
        }
        Optional<TypeParkingDTO> Type = typeParkingService.findOne(idType);

            typeParkingService.delete(idType);
        return ResponseEntity.ok("The parking n°"+idType+" was deleted.");


    }




}
