package com.gfi.parking.parking.controllers;


import com.gfi.parking.parking.DTO.PersonneDTO;
import com.gfi.parking.parking.service.PersonneService;
import io.swagger.models.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.Collections;
import java.util.Map;

@RestController
@CrossOrigin(origins = "*")
public class LoginController {

    @Autowired
    private PersonneService userService;

    @RequestMapping("/v1/token")
    public Map<String, String> token(HttpSession session, HttpServletRequest request){

        System.out.println(request.getRemoteHost());

        System.out.println("Authorization"+request.getHeader("Authorization"));

        String remoteHost = request.getRemoteHost();

        int portNumber = request.getRemotePort();

        System.out.println(remoteHost + ":" + portNumber);
        System.out.println(request.getRemoteAddr());

        return Collections.singletonMap("token", session.getId());
    }

    @RequestMapping("/v1/checkSession")
    public ResponseEntity checkSession(){

        //SecurityContextHolder.getContext();
       // return new ResponseEntity("Session Active! ", HttpStatus.OK);
        return ResponseEntity.ok(SecurityContextHolder.getContext());
    }


    @RequestMapping(value="/v1/user/logout", method= RequestMethod.POST)
    public ResponseEntity logout(){
        SecurityContextHolder.clearContext();
        return new ResponseEntity(HttpStatus.OK);



    }



}
