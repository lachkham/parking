package com.gfi.parking.parking.controllers;


import com.gfi.parking.parking.DTO.ParkingDTO;
import com.gfi.parking.parking.entities.Personne;
import com.gfi.parking.parking.repository.PersonneRepository;
import com.gfi.parking.parking.service.ParkingService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.Optional;

@RestController
@RequestMapping("/v1/parking")
@CrossOrigin(origins = "*")

public class ParkingController {

    private final ParkingService parkingService;
    private final PersonneRepository personneService;


    public ParkingController(ParkingService parkingService, PersonneRepository personneService) {
        this.parkingService = parkingService;
        this.personneService = personneService;
    }

    @GetMapping("/Get")
    public ResponseEntity findAll(){
        return ResponseEntity.ok(parkingService.findAll());
    }

    @GetMapping("/All/ByOwner")
    public ResponseEntity findAllOwner(HttpServletRequest request){
        Personne personne = personneService.findByUsername(request.getRemoteUser());

        return ResponseEntity.ok(parkingService.findAllOwner(personne.getSuperior()));
    }
    @GetMapping("/invalid")
    public ResponseEntity findAllinvalid(HttpServletRequest request){
        Personne personne = personneService.findByUsername(request.getRemoteUser());

        return ResponseEntity.ok(parkingService.findAllinvalid(personne.getSuperior())    );
    }
    @GetMapping("/Get/{idPark}")
    public  ResponseEntity findParkingById(@PathVariable Long idPark){
        if (idPark==null){
            return ResponseEntity.badRequest().body("Empty request parameter");
        }
        Optional<ParkingDTO> parking= parkingService.findOne(idPark);

        if (!parking.isPresent()){
            return ResponseEntity.notFound().build();

        }
        return ResponseEntity.ok(parking);
    }


    @GetMapping("/Get/{longitude}/{latitude}")
    public  ResponseEntity findParkingByLongLatt(@PathVariable double longitude, @PathVariable double latitude){
        if ((longitude == 0) || (latitude == 0)){
            return ResponseEntity.badRequest().body("Empty request parameter (longitude="+longitude+" latitude="+latitude+")");
        }
        Optional<ParkingDTO> parking= parkingService.findByLongitudeAndLatitude(longitude,latitude);
        if (!parking.isPresent()){
            return ResponseEntity.notFound().build();

        }

        return ResponseEntity.ok(parking);
    }


    @PostMapping("/")
    public ResponseEntity createParking(@RequestBody ParkingDTO parking,HttpServletRequest request){
        Personne personne = personneService.findByUsername(request.getRemoteUser());
        System.out.println(personne.getCin());
        if(personne!=null)
        parking.setOwner(personne.getSuperior());

        System.out.println(parking);

        if (parking==null){
            return ResponseEntity.badRequest().body("Null Parking");
        }
        ParkingDTO createParking = parkingService.save(parking);

        return ResponseEntity.ok().body(createParking);
    }

    @DeleteMapping("/{idPark}")
    public  ResponseEntity DeleteParking (@PathVariable Long idPark){
        if (idPark==null){
            return ResponseEntity.badRequest().body("Null id parking");
        }
        Optional<ParkingDTO> parking = parkingService.findOne(idPark);

        parkingService.delete(idPark);
        return ResponseEntity.ok(parking);

    }

    @GetMapping("/count/invalid")
    public ResponseEntity countAllInvalidParking(HttpServletRequest request){
        Personne personne = personneService.findByUsername(request.getRemoteUser());

        return ResponseEntity.ok(parkingService.countByInvalidParkings(personne.getSuperior()));
    }
    @GetMapping("/count/valid")
    public ResponseEntity countAllValidParking(HttpServletRequest request){
        Personne personne = personneService.findByUsername(request.getRemoteUser());

        return ResponseEntity.ok(parkingService.countValidParking(personne.getSuperior()));
    }

    @GetMapping("/validation/{idPark}")
    public  ResponseEntity ValiderById(@PathVariable Long idPark){
        if (idPark==null){
            return ResponseEntity.badRequest().body("Empty request parameter!");
        }
        Optional<ParkingDTO> parking= parkingService.valider(idPark);

        if (!parking.isPresent()){
            return ResponseEntity.notFound().build();
        }

        return ResponseEntity.ok(parking);
    }

    @GetMapping("/nbPlace")
    public ResponseEntity totalPlaceParking(HttpServletRequest request){
        Personne personne = personneService.findByUsername(request.getRemoteUser());

        return ResponseEntity.ok(parkingService.totalPlace(personne.getSuperior()));
    }


}
