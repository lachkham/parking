package com.gfi.parking.parking.controllers;


import com.gfi.parking.parking.DTO.AdresseDTO;
import com.gfi.parking.parking.DTO.ReservationDTO;
import com.gfi.parking.parking.entities.Parking;
import com.gfi.parking.parking.entities.Personne;
import com.gfi.parking.parking.repository.ParkingRepository;
import com.gfi.parking.parking.repository.PersonneRepository;
import com.gfi.parking.parking.service.AdresseService;
import com.gfi.parking.parking.service.ReservationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Optional;

@RestController
@RequestMapping("/v1/reservation")
@CrossOrigin(origins = "*")
public class ReservationController {
    private final ReservationService reservationService;
    private final PersonneRepository personneService;
    private  final ParkingRepository parkingRepository;
    @Autowired
    private JavaMailSender javaMailSender;

    public ReservationController(ReservationService reservationService, PersonneRepository personneService, ParkingRepository parkingRepository) {
        this.reservationService = reservationService;
        this.personneService = personneService;
        this.parkingRepository = parkingRepository;
    }


    @GetMapping("/")
    public ResponseEntity findAll(HttpServletRequest request){
      //  System.out.println(request.getRemoteUser());
        Personne personne = personneService.findByUsername(request.getRemoteUser());

        return ResponseEntity.ok(reservationService.findAll(personne.getSuperior()));
    }
    @GetMapping("/{idType}")
    public ResponseEntity findByID(@PathVariable Long idType){
        if (idType== null){
            return ResponseEntity.badRequest().body("Null Identifier");
        }
        Optional<ReservationDTO> reservationDTO = reservationService.findOne(idType);
        if (!reservationDTO.isPresent())
            return ResponseEntity.notFound().build();

        return ResponseEntity.ok(reservationDTO);
    }

    @PostMapping("/")
    public ResponseEntity createReservation(@RequestBody ReservationDTO reservation){
        if(reservation==null){
           return ResponseEntity.badRequest().body("Empty Request Body");
        }

        ReservationDTO reservationDTO = reservationService.save(reservation);
        Parking        parking     = parkingRepository.findById(reservation.getIdParking()).get();
        DateFormat simple = new SimpleDateFormat("dd M yyyy  HH:mm");

        SimpleMailMessage msg = new SimpleMailMessage();
        msg.setTo(reservationDTO.getEmail());
        msg.setSubject("Réservation place parking N°"+reservationDTO.getIdReservation());
        msg.setText("Bonjour, \n \n" +
                "Vous avez réserver une place à "+parking.getLibelle()+", situé à "+parking.getAdresse().getAdresse()+"" +
                        ", "+parking.getAdresse().getVille()+", "+parking.getAdresse().getCodePostal()+", "+parking.getAdresse().getGouvernorat()+", "+parking.getAdresse().getPays()+
                " \nLe " + simple.format(reservation.getEntre()) +" à "+ simple.format(reservation.getSortie())+
                ".\n\n" +
                "Le numero de votre réservation est: " +reservationDTO.getIdReservation()+"\n\n" +
                "Notez Bien: vous devez présenter ce mail pour bénéficier de votre place parking.\n" +
                "\nCordialement");
        javaMailSender.send(msg);

        return ResponseEntity.ok(reservationDTO);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity deleteReservation(@PathVariable Long id){
        if (id==null){
            return ResponseEntity.badRequest().body("Null id ");
        }
        Optional<ReservationDTO> reservation = reservationService.findOne(id);

        reservationService.delete(id);
        if (reservation.isPresent()){
           ReservationDTO reservationDTO = reservation.get();

        SimpleMailMessage msg = new SimpleMailMessage();
        msg.setTo(reservationDTO.getEmail());
        msg.setSubject("Réservation annulée");
        msg.setText("Bonjour, \n \n" +

                "Votre Réservation N°" +reservationDTO.getIdReservation()+" a été annulée.\n"+

                "\nCordialement");
        javaMailSender.send(msg);
        }

        return ResponseEntity.ok("");

    }
    @GetMapping("/Get/{idPark}/{entre}/{sortie}")
    public ResponseEntity countReservation(@PathVariable Long idPark,@PathVariable Long entre,@PathVariable Long sortie){
        if(entre==null||sortie==null){
            return ResponseEntity.badRequest().body("null Request");
        }
        Optional<Long> nbReservation = reservationService.nbReservation(idPark,entre,sortie);
        return ResponseEntity.ok(nbReservation);
    }
    @GetMapping("ParkingActifs/{entre}/{sortie}")
    public ResponseEntity countReservation(@PathVariable Long entre,@PathVariable Long sortie,HttpServletRequest request){
        Personne personne = personneService.findByUsername(request.getRemoteUser());

        if(entre==null||sortie==null){
            return ResponseEntity.badRequest().body("null Request");
        }
        Optional<Long> nbParking = reservationService.countParkinActifs(entre,sortie,personne.getSuperior());
        return ResponseEntity.ok(nbParking);
    }
}
