package com.gfi.parking.parking.controllers;


import com.gfi.parking.parking.DTO.ParkingDTO;
import com.gfi.parking.parking.DTO.PersonneDTO;
import com.gfi.parking.parking.service.PersonneService;
import org.springframework.http.ResponseEntity;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.Optional;

@RestController
@RequestMapping("/v1/personnes")
@CrossOrigin(origins = "*")

public class PersonneController {


    private final PersonneService personneService;

    public PersonneController(PersonneService personneService) {
        this.personneService = personneService;
    }

    //Get All Personne
    @GetMapping("/")
    public ResponseEntity findAll(HttpServletRequest request){

        return  ResponseEntity.ok(personneService.findAll(request.getRemoteUser()));
    }

    //Get personne By CIN
    @GetMapping("/{cin}")
    public ResponseEntity   findPersonneById(@PathVariable( name= "cin" ) String cin){

        if (cin==null)
            return ResponseEntity.badRequest().body("Error CIN is null");

        Optional<PersonneDTO> personne =personneService.findOne(cin);

        if (!personne.isPresent())
            return ResponseEntity.notFound().build();

        return ResponseEntity.ok().body(personne);
    }
    @GetMapping("/getCurrentUser")
    public ResponseEntity getCurrentUser(HttpServletRequest request){

        Optional<PersonneDTO> personneDTO = personneService.findByUsername(request.getRemoteUser());
        if(!personneDTO.isPresent())
            return  ResponseEntity.notFound().build();
        return ResponseEntity.ok(personneDTO);
    }

    //Add personne
    @PostMapping("/")
    public ResponseEntity createPersonne(@RequestBody PersonneDTO personne){

        if (personne==null )
            return ResponseEntity.badRequest().body("Cannot create NULL personne");

            PersonneDTO createPersonne= personneService.save(personne);
        if(createPersonne!=null)
            return ResponseEntity.ok(createPersonne);
        else
            return ResponseEntity.badRequest().body("Username Exist");


    }

    @DeleteMapping("/{cin}")
    public  ResponseEntity DeleteParking (@PathVariable String cin){
        if (cin==null){
            return ResponseEntity.badRequest().body("Null CIN");
        }
        Optional<PersonneDTO> parking = personneService.findOne(cin);

        personneService.delete(cin);
        return ResponseEntity.ok(parking);
    }

}
